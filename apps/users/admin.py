from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from users.models import User


@admin.register(User)
class Admin(UserAdmin):
    ordering = ['-id']
    list_display = ['id', 'email', 'name']


