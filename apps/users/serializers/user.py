import re
from django.contrib.auth.models import update_last_login
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from users.models import User


class UserDataSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=False, validators=[validate_password])

    class Meta:
        model = User
        fields = [
            'id',
            'name',
            'email',
            'is_superuser',
            'role',
            'password',
            'photo',
        ]


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):  # noqa
    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['user_data'] = UserDataSerializer(self.user).data

        update_last_login(None, self.user)

        return data


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=255, required=True, write_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'email',
            'password',
        ]

    @staticmethod
    def validate_password(value):
        if bool(re.fullmatch(r'^(?=.*[a-zA-Z])(?=.*\d)[ -~]{8,}$', value)):
            return value
        raise serializers.ValidationError('Password must be at least 8 characters long and '
                                          'contain at least one number and letter of any case.')

    @transaction.atomic
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        user = super().create(validated_data)
        if password:
            user.set_password(password)
            user.save()

        return user


class UserUpdateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=255, required=True, write_only=True)
    name = serializers.CharField(required=False)

    class Meta:
        model = User
        fields = [
            'id',
            'email',
            'password',
            'name',
            'role'
        ]

    @staticmethod
    def validate_password(value):
        if bool(re.fullmatch(r'^(?=.*[a-zA-Z])(?=.*\d)[ -~]{8,}$', value)):
            return value
        raise serializers.ValidationError('Password must be at least 8 characters long and '
                                          'contain at least one number and letter of any case.')

    @transaction.atomic
    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        user = super().update(instance, validated_data)

        if password:
            user.set_password(password)
            user.save()

        return user


class UserShortSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'id',
            'name',
            'email',
            'photo',
        ]
