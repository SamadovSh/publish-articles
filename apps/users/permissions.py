from rest_framework.permissions import BasePermission


class IsSuperUser(BasePermission):

    def has_permission(self, request, view):
        if request.method in ['POST', 'GET']:
            return True
        if request.user and request.user.is_authenticated:
            _id = view.kwargs.get('pk')
            return request.user.is_superuser or str(request.user.id) == _id

        return False
