import datetime

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from users.models import User
from users.permissions import IsSuperUser
from users.serializers.user import UserCreateSerializer, UserDataSerializer, UserUpdateSerializer


class UserViewSet(ModelViewSet):
    queryset = User.objects.order_by('-date_joined')
    filter_backends = [SearchFilter, DjangoFilterBackend]
    filterset_fields = ['role']
    ordering = ['-date_joined']
    search_fields = ['email', 'name']
    permission_classes = [IsSuperUser, ]

    def get_serializer_class(self):
        if self.action == 'create':
            return UserCreateSerializer
        elif self.action in ['partial_update', 'update']:
            return UserUpdateSerializer

        return UserDataSerializer

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.paginator.get_next_link(),
                'previous': self.paginator.get_previous_link()
            },
            'next': self.paginator.page.next_page_number() if self.paginator.page.paginator.num_pages > self.paginator.page.number else None,
            'previous': self.paginator.page.previous_page_number() if self.paginator.page.number > 1 else None,
            'count': self.paginator.page.paginator.count,
            'results': data
        })

    @action(['GET'], detail=False)
    def log_out(self, request):
        user = request.user
        now = datetime.datetime.now()

        user.last_logout = now
        user.save()

        return Response(status=200)

