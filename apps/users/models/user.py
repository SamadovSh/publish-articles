from django.db import models
from users.models.abstract_user import AbstractUser


class Role(models.TextChoices):
    SUBSCRIBER = "subscriber"
    AUTHOR = "author"


class User(AbstractUser):
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=256)
    name = models.CharField(max_length=512, null=True)
    photo = models.ImageField(upload_to="users/images/", blank=True, null=True)
    role = models.CharField(max_length=64, choices=Role.choices, default=Role.SUBSCRIBER)
    last_activity = models.DateTimeField(null=True, blank=True)

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']



