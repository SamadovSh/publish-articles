from django.db.models import Model, DateTimeField


class BaseModel(Model):
    created_date = DateTimeField(auto_now_add=True)
    modified_date = DateTimeField(auto_now=True)

    class Meta:
        abstract = True


