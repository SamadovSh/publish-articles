from django.urls import path, include

urlpatterns = [
    path('users/', include(('users.urls', 'users'), 'users'), ),
    path('articles/', include(('articles.urls', 'articles'), 'articles'), ),
]
