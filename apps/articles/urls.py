from django.urls import path, include
from rest_framework.routers import DefaultRouter

from articles.views.article import ArticleViewSet
from articles.views.tag import TagViewSet

router = DefaultRouter()
router.register('tags', TagViewSet, 'tags')
router.register('articles', ArticleViewSet, 'articles')

urlpatterns = [
    path('', include(router.urls)),
]
