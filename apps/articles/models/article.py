from django.db import models

from articles.models.tag import Tag
from custom.model import BaseModel
from users.models import User


class Article(BaseModel):
    title = models.CharField(max_length=1028)
    description = models.TextField()
    tags = models.ManyToManyField(Tag)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='articles')
    views = models.IntegerField(default=0)
    public = models.BooleanField(default=True)

    class Meta:
        ordering = ('-id',)
