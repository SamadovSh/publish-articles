from django.db import models


class Tag(models.Model):
    title = models.CharField(max_length=256)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ('-id',)
