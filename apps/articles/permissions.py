from rest_framework.permissions import BasePermission

from articles.models import Article


class IsAllowed(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.user and request.user.is_authenticated:
            if request.method in ['PUT', 'PATCH', 'DELETE']:
                _id = view.kwargs.get('pk')
                is_author = Article.objects.filter(id=_id, author=request.user)
                return is_author.exists()
            else:
                return True
        else:
            return request.method == 'GET'
