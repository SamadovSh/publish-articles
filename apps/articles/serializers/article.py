from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from articles.models import Article
from articles.serializers.tag import TagSerializer
from users.models.user import Role
from users.serializers.user import UserShortSerializer


class ArticleCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = [
            'id',
            'title',
            'description',
            'tags',
            'author',
            'public'
        ]
        extra_kwargs = {"tags": {"required": False},
                        "author": {"required": False},
                        "public": {"required": False}}

    @transaction.atomic
    def create(self, validated_data):
        user = self.context['request'].user
        if user.is_authenticated and (user.role == Role.AUTHOR or user.is_superuser):
            validated_data['author'] = user
            instance = super(ArticleCreateSerializer, self).create(validated_data)
            return instance

        raise PermissionDenied({"message": "You don't have permission to access"})


class ArticleModelSerializer(serializers.ModelSerializer):
    author = UserShortSerializer()
    tags = TagSerializer(many=True)

    class Meta:
        model = Article
        fields = [
            'id',
            'title',
            'description',
            'tags',
            'author',
            'public',
            'views'
        ]
