from rest_framework import serializers

from articles.models import Tag


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = [
            'id',
            'title',
            'is_active'
        ]
